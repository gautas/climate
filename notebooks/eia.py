# checking eia processing

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from pygsutils import general as g, cache as c
import logging

# %%
loc = "./../../data/eia"
fp_log = f"{loc}/log_process_eia.log"

# %%
g.setup_logging(fp=fp_log, level=logging.INFO)
c.set_loc(f"{loc}/intermediate")


# %%
from climate.eia import process as p

# %%
genfuel = p.GenFuel(loc=loc)

# %%
df = genfuel.df


# %%
for i in df.columns:
    print(i)

# %%
num_rows = df[["year", "general_fuel_type"]].fillna("None").assign(num_rows=1).groupby(["year", "general_fuel_type"], as_index=False).agg({"num_rows": "sum"})

# %%
num_rows.sort_values("num_rows", ascending=False)

# %%
df.head()

# %%
import calendar

# %%
for i in calendar.month_abbr:
    print(i)

# %%
from climate.eia import base as b

# %%
len(b.month_abbrs)

# %%
EIA = b.EIA(loc=loc)

# %%
for yr in EIA.years:
    print(yr.year)

# %%
yr2011 = [yr for yr in EIA.years if yr.year == 2011][0]

# %%
yr2011.field_renames

# %%

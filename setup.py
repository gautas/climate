import setuptools


with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="climate",
    version="0.0.1",
    author="Gautam Sisodia",
    packages=setuptools.find_packages(),
    classifiers=["Progamming Language :: Python :: 3"],
    dependency_links=['http://github.com/gautsi/pygsutils/tarball/master#egg=pygsutils-0.0.1'],
    install_requires=[
        "requests==2.25.1",
        "ipython==7.22.0",
        "beautifulsoup4==4.9.3",
        "pygsutils @ git+http://github.com/gautsi/pygsutils#egg=pygsutils",
        "pydantic==1.8.1",
        "geopandas==0.9.0",
        "xlrd==2.0.1",
        "openpyxl==3.0.7",
        "arcgis==1.8.5.post3",
        "setuptools==56.0.0",
        "python-libarchive==4.0.1.post1",
        "altair==4.1.0",
    ]
)
